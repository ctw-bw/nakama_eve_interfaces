
# MSG 

## ArucoData.msg
Output data of python opencv Aruco library

* marker_ids
Unbounded array (int64) of marker id's, one for each marker observed

* bounding_boxes
Unbounded array (int64) of bounding box coordinates. Every 8 coordinates correspond to the bounding box of one detected marker. The order is:
    * left_upper_x, left_upper_y, 

* translation_vectors
Unbounded array (float64) of translation coordinates. Every 3 coordinates correspond to the translation of one detected marker. The order is:
    * translation_x: ?? axis of the marker
    * translation_y: ?? axis of the marker
    * translation_z: ?? axis of the marker

* rotation_vectors
Unbounded array (float64) of rotation coordinates. Every 3 coordinates correspond to the rotation of one detected marker. The order is:
    * rotation_x: ?? axis of the marker
    * rotation_y: ?? axis of the marker
    * rotation_z: ?? axis of the marker
